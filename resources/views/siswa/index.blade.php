@extends('layouts.master')

@section('content')
		@if(session('sukses'))
		<div class="alert alert-secondary" role="alert">
  			Berhasil
  		</div>
  		@endif
		<div class="row">
			<div class="col-6">
				<h1>TABEL DATA SISWA</h1>
			</div>
			<div class="col-6"></div>

				<button type="button" class="btn btn-secondary btn-sm btn-block" data-toggle="modal" data-target="#exampleModal">
				  Tambahkan	
				</button>

				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="exampleModalLabel">Data Siswa</h5>
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				        <form action="/siswa/create" method="POST">
				        	{{csrf_field(	)}}
							  <div class="form-group">
							    <label for="usr">Nama Depan</label>
							    <input name="nama_depan" type="text" class="form-control" id="usr" aria-describedby="emailHelp" placeholder="Nama Depan">
							  </div>
							  <div class="form-group">
							    <label for="usr">Nama Belakang</label>
							    <input name="nama_belakang" type="text" class="form-control" id="usr" aria-describedby="emailHelp" placeholder="Nama Belakang">
							  </div>
							  <div class="form-group">
							    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
							    <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
							      <option value="L">Laki-Laki</option>
							      <option value="P">Perempuan</option>
							    </select>
							  </div>
							   <div class="form-group">
							   	 <label for="usr">Agama</label>
							    	<input name="agama" type="text" class="form-control" id="usr" aria-describedby="emailHelp" placeholder="Agama">
							  </div>
							  <div class="form-group">
						   		 <label for="exampleFormControlTextarea1">Alamat</label>
						    		<textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
						  </div>
									      
					</div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				        <button type="submit" class="btn btn-secondary">Submit</button>
				      </form>
				      </div>
				    </div>
				  </div>
				</div>
			
				<table class="table table-hover">
					<thead class="thead-dark">
					<tr>
						<th>Nama Depan</th>
						<th>Nama Belakang</th>
						<th>Jenis Kelamin</th>
						<th>Agama</th>
						<th>Alamat</th>
						<th>Aksi</th>
					</tr>
					</thead>
					<tbody>
					@foreach($data_siswa as $siswa)
					<tr>
						<td>{{$siswa->nama_depan}}</td>
						<td>{{$siswa->nama_belakang}}</td>
						<td>{{$siswa->jenis_kelamin}}</td>
						<td>{{$siswa->agama}}</td>
						<td>{{$siswa->alamat}}</td>
						<td><a href="\siswa\{{$siswa->id}}/edit" class="btn btn-dark btn-sm">Edit</a>
							<a href="/siswa/{{$siswa->id}}/delete" class="btn btn-secondary btn-sm" onclick=" return confirm('Anda yakin?')">Delete</a>
						</td>
					</tr>
					@endforeach
					</tbody>
				</table> 
		</div>
	</div>

@endsection
	
